<?php
/**
 * Curse Inc.
 * ArticlePageDataDiag
 * ArticlePageDataDiag Hooks
 *
 * @author		Alex Smith
 * @copyright	(c) 2013 Curse Inc.
 * @license		Proprietary
 * @package		ArticlePageDataDiag
 * @link		https://gitlab.com/hydrawiki
 *
 **/
namespace ArticlePageDataDiag;

class Hooks {
	/**
	 * Article Page Fields
	 *
	 * @var		mixed
	 */
	public static $articlePageFields;

	/**
	 * Article Page Data
	 *
	 * @var		mixed
	 */
	public static $articlePageData;

	/**
	 * Captures the parameters for an article page select.
	 *
	 * @access	public
	 * @param	object	&$wikiPage WikiPage object passed as a reference.
	 * @param	array	$fields Fields that are being selected from the database.
	 * @return	boolean	True
	 */
	public static function onArticlePageDataBefore(WikiPage &$wikiPage, $fields) {
		self::$articlePageFields = $fields;

		return true;
	}

	/**
	 * Captures the database row from an article page select.
	 *
	 * @access	public
	 * @param	object	&$wikiPage WikiPage object passed as a reference.
	 * @param	mixed	$row Database row that was selected or nothing at all.
	 * @return	boolean	True
	 */
	public static function onArticlePageDataAfter(WikiPage &$wikiPage, $row) {
		self::$articlePageData = $row;

		return true;
	}

	/**
	 * Returns the article page data.
	 *
	 * @access	public
	 * @return	boolean	mixed
	 */
	public static function getArticlePageFields() {
		return self::$articlePageFields;
	}

	/**
	 * Returns the article page data.
	 *
	 * @access	public
	 * @return	boolean	mixed
	 */
	public static function getArticlePageData() {
		return self::$articlePageData;
	}
}
