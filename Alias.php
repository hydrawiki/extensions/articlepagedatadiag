<?php
/**
 * Curse Inc.
 * ArticlePageDataDiag
 * ArticlePageDataDiag Aliases
 *
 * @author 		Brent Copdeland
 * @copyright	(c) 2013 Curse Inc.
 * @license		Proprietary
 * @package		ArticlePageDataDiag
 * @link		https://gitlab.com/hydrawiki
 *
 **/

$specialPageAliases = [];

/** English (English) */
$specialPageAliases['en'] = [];

/**
 * For backwards compatibility with MediaWiki 1.15 and earlier.
 */
$aliases =& $specialPageAliases;
