<?php
/**
 * Curse Inc.
 * ArticlePageDataDiag
 * ArticlePageDataDiag Mediawiki Settings
 *
 * @author		Alexia E. Smith
 * @copyright	(c) 2013 Curse Inc.
 * @license		Proprietary
 * @package		ArticlePageDataDiag
 * @link		https://gitlab.com/hydrawiki
 *
 **/

if (function_exists('wfLoadExtension')) {
	wfLoadExtension('ArticlePageDataDiag');
	wfWarn(
		'Deprecated PHP entry point used for ArticlePageDataDiag extension. ' .
		'Please use wfLoadExtension instead, ' .
		'see https://www.mediawiki.org/wiki/Extension_registration for more details.'
	);
	return;
} else {
	die('This version of the ArticlePageDataDiag extension requires MediaWiki 1.25+');
}
